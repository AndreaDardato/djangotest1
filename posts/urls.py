from django.urls import path
from . import views as posts_views

#definiremo 3 schemi url (lista post, post singoli, sezione contatti)

urlpatterns = [
    path("", posts_views.lista_posts, name="lista"),
    path("post-singolo/", posts_views.post_singolo, name="singolo"),
    path("contatti/", posts_views.contatti, name="contatti")
]



