from django.db import models

# Create your models here.

#andimao a dire come vogliamo che la nostra app sia
class Post(models.Model):
    #descrivo come deve ussere un post
    titolo = models.CharField(max_length = 120)
    contenuto = models.TextField()
    data = models.DateTimeField(auto_now=False, auto_now_add=True)
    slug = models.SlugField()
    #il modello creato è come una tabella del DB

    def __str__(self):
        return self.titolo  #ritorno il titolo del post


